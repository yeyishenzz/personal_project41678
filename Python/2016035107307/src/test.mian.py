from main import *
import unittest
class Testyunsuan(unittest.TestCase):
    def test_add(self):
        ta = yunsuan("3+1")
        self.assertEqual(ta,'4')
    def test_reduce(self):
        ta = yunsuan("6-2")
        self.assertEqual(ta,'4')
    def test_ride(self):
        ta = yunsuan("6*2")
        self.assertEqual(ta,'12')
    def test_except(self):
        ta = yunsuan("6/3")
        self.assertEqual(ta,'2.0')
    def test_kuohao(self):
        ta = yunsuan("(6-3)*2")
        self.assertEqual(ta,'6')
    def test_kuohao(self):
        ta = yunsuan("6/0")
        self.assertEqual(ta,'分母不能为0')
    def test_add1(self):
        self.assertRaises(BaseException,yunsuan,("-1+2"))
    def test_reduce1(self):
        self.assertRaises(BaseException,yunsuan,("-1-2"))
    def test_ride1(self):
        self.assertRaises(BaseException,yunsuan,("-1*2"))
    def test_except(self):
        self.assertRaises(BaseException,yunsuan,("-1/2"))
if __name__ == "__main__":
    unittest.main()
